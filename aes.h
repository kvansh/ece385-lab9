#include <stdio.h>
#define uchar unsigned char
#define uint unsigned long


extern const uchar aes_sbox[][16];
extern const uchar aes_invsbox[][16];
extern uchar gf_mul[][6];
extern uint Rcon[];


