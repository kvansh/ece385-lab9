#include "key_expansion.h"
using namespace std;

void Word(uchar *w, uchar* temp){
    int j;

    for(j=0; j<4; ++j)
        temp[j] = w[j];
}

void _Xor(uchar* A, uchar* B, uchar* C){
    int i = 0;
    for(i=0; i<4; ++i)
        C[i] = A[i] xor B[i];
}

void copyArr(uchar (*A)[4], uchar *B){
    int i, j, m;
    m = 0;
    for(i=0; i<4; ++i){
        for(j=0; j<Nb; ++j){
            A[i][j] = B[m];
            m = m + 1;
        }
    }
}

void PrintAll(uchar (*A)[4]){
    int i,j;
    for(i=0; i<4; ++i){
        for(j=0; j<4; ++j){
            printf("%x ", A[i][j]);
        }
        printf("\n");
    }
}

void shiftRows(uchar (*matrix)[4]){
    int i;
    for(i=1; i<4; ++i)
        rotate(matrix[i], i);
}

void mixColumns(uchar (*matrix)[4]){
    uchar temp[4], copy[4];
    int i, j;
    
    for(i=0; i<4; ++i){
        copy[0] = matrix[0][i];
        copy[1] = matrix[1][i];
        copy[2] = matrix[2][i];
        copy[3] = matrix[3][i];

        temp[0] = copy[0] & 0x80 ? (copy[0]<<1) ^ 0x1b : copy[0]<<1;
        temp[1] = copy[1] & 0x80 ? (copy[1]<<1) ^ 0x1b : copy[1]<<1;
        temp[2] = copy[2] & 0x80 ? (copy[2]<<1) ^ 0x1b : copy[2]<<1;
        temp[3] = copy[3] & 0x80 ? (copy[3]<<1) ^ 0x1b : copy[3]<<1;
        
        matrix[0][i] = temp[0]^copy[3]^copy[2]^temp[1]^copy[1];
        matrix[1][i] = temp[1]^copy[0]^copy[3]^temp[2]^copy[2];
        matrix[2][i] = temp[2]^copy[1]^copy[0]^temp[3]^copy[3];
        matrix[3][i] = temp[3]^copy[2]^copy[1]^temp[0]^copy[0];
    }
}
/*
void mixColumns(uchar (*matrix)[4]){
    uchar result[4][4];
    int j, i;
    
    for(j=0; j<4; ++j)
        mixColumn(matrix, j, result);

    // Copy back to state
    for(i=0; i<4; i++){
        for(j=0; j<4; j++){
            matrix[i][j] = result[i][j];
        }
    }
}

void mixColumn(uchar (*matrix)[4], int j, uchar (*result)[4]){
    int i;
    uchar temp[4];
    
    temp[0] = gf_mul[int(matrix[0][j])][0] xor gf_mul[matrix[1][j]][1] xor matrix[2][j] xor matrix[3][j];
    temp[1] = matrix[0][j] xor gf_mul[matrix[1][j]][0] xor gf_mul[int(matrix[2][j])][1] xor matrix[3][j];
    temp[2] = matrix[0][j] xor matrix[1][j] xor gf_mul[int(matrix[2][j])][0] xor gf_mul[int(matrix[3][j])][1];
    temp[3] = gf_mul[int(matrix[0][j])][1] xor matrix[1][j] xor matrix[2][j] xor gf_mul[int(matrix[3][j])][0];
 
    for(i = 0; i<4; ++i)
        result[i][j] = temp[i];
}
*/
void transpose(uchar (*source)[4], int start){
    int i, j;
    uchar dest[4][4];
    for(i=0; i<4; i++){
        for(j=0; j<4; j++){
            dest[j][i] = source[i+start][j];
        }
    }
    //copy back to source
    for(i=0; i<4; ++i){
        for(j=0; j<4; j++){
            source[i+start][j] = dest[i][j];   
        }
    }    
}

void addRoundKey(uchar (*state)[4], uchar (*roundKey)[4], int start){
    uchar result[4][Nb];
    int i,j;
    
    transpose(roundKey, start);
    
    printf("ROUND KEY: \n");
    for(i=0; i<4; i++){
        _Xor(state[i], roundKey[start+i], result[i]);
        printf("%x %x %x %x\n", roundKey[start+i][0], roundKey[start+i][1], roundKey[start+i][2], roundKey[start+i][3]);
    }
    printf("============\n");
    // Copy back to state
    for(i=0; i<4; i++){
        for(j=0; j<4; j++){
            state[i][j] = result[i][j];
        }
    }
}

void subBytes(uchar *rotword){
    int i;
    uchar upper4, lower4;

    for (i = 0; i<4; ++i){
        upper4 = (int)(rotword[i]) & 0xF0;
        upper4 = upper4>>4;

        lower4 = (int)(rotword[i]) & 0x0F;
        rotword[i] = aes_sbox[int(upper4)][int(lower4)];
    }
}

void subBytes(uchar (*state)[4]){
    int i,j;
    uchar upper4, lower4;

    for (i = 0; i<4; ++i){
        for (j=0; j<4; ++j){
            upper4 = (int)(state[i][j]) & 0xF0;
            upper4 = upper4>>4;

            lower4 = (int)(state[i][j]) & 0x0F;
            state[i][j] = aes_sbox[int(upper4)][int(lower4)];
        }
    }
}

void rcon(uchar* rotword, int count){
    int i, shift, temp;
    uchar _xor;
    shift = 24;
    
    for(i=0; i<4; ++i){
        temp = Rcon[count]>>shift;
        _xor = rotword[i] xor (uchar)(temp & 0xFF); 
        //_xor = _xor xor ckey[i][0];
        //printf("rot: %x, aft: %x\n", rotword[i], _xor);
        rotword[i] = _xor;
        shift -= 8;
    }
}

void rotate(uchar *row, int count){
    uchar ele;
    int i;
    while(count >0){
        ele = row[0];
        for(i=0; i<3; i++)
            row[i] = row[(i+1)%4];
        row[3] = ele;
        count -= 1;
    }
}

void KeyExpansion(uchar key[16], uchar (*w)[4]){
    uchar temp[4];
    int i,j;
    i = 0;
    j = 0;
    
    //transpose(

    while (i < Nk){
        //Word(key, w[i]);
        for(j=0; j<4; ++j)
            w[i][j] = key[(4*i)+ j];
        i = i+1;
    } 
    
    i = Nk;

    while (i < (Nb * (Nr+1))){
        Word(w[i-1], temp);

        if (i % Nk == 0){
            rotate(temp,1);
            subBytes(temp);
            rcon(temp, (i/Nk)-1); 
        }

        _Xor(w[i-Nk], temp, w[i]);
        i++;
    }
}

void AES(uchar in[4*Nb], uchar (*out)[Nb], uchar (*w)[4]){
    uchar state[4][4];
    int round, i, j;
    copyArr(state, in);
    
    addRoundKey(state, w, 0);
    
//    PrintAll(state);
//    printf("END OF ROUND 0-------------\n");

    for(round = 1; round<(Nr); round++){
        subBytes(state);
        
//        printf("SUB BYTES: \n");
//        PrintAll(state);
        
        shiftRows(state);
        
//        printf("SHIFT ROWS: \n");
//        PrintAll(state);
        
        mixColumns(state);

//        printf("MIX COLUMNS: \n");
//        PrintAll(state);

        addRoundKey(state, w, (round*Nb));
        
//        printf("ADD ROUND KEY: \n");
//        PrintAll(state);
        
//        printf("END OF ROUND %i------------\n", round);
    }

    subBytes(state);
    shiftRows(state);
    addRoundKey(state, w, (Nr)*Nb);
//    PrintAll(state);
//    printf("-------------\n");
    // Copy to out
    for(i=0; i<4; i++){
        for(j=0; j<4; j++){
            out[i][j] = state[i][j];
        }
    }
}

void PrintAllW(uchar (*w)[4]){
    int i,j;
    printf("fgiuhortgjhiorjgbo909i0\n");
    for(i=0; i<TOTAL; ++i){
        for(j=0; j<4; ++j){
            printf("%x", w[i][j]);
        }
        //printf("\n");
    }
    printf("\nfgiuhortgjhiorjgbo909i0\n");
}

int main(){
    uchar out[4][Nb];
#if 1
    uchar cypher_key[16] = 
    {
    0x00, 0X01, 0X02, 0X03,
    0X04, 0X05, 0X06, 0X07,
    0X08, 0X09, 0X0a, 0X0b,
    0X0c, 0X0d, 0X0e, 0X0f
    };
#endif
#if 0
    uchar in[16] =
    {
        0xec, 0xe2, 0x98, 0xdc,
        0xec, 0xe2, 0x98, 0xdc,
        0xec, 0xe2, 0x98, 0xdc,
        0xec, 0xe2, 0x98, 0xdc
    };
#endif
#if  1 
    uchar in[16] =
    {
        0xec, 0xec, 0xec, 0xec,
        0xe2, 0xe2, 0xe2, 0xe2,
        0x98, 0x98, 0x98, 0x98,
        0xdc, 0xdc, 0xdc, 0xdc
    };
#endif
//    uchar in[16] = {0x00, 0x01, 0x02, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37,0x07, 0x34};
//    uchar cypher_key[16] = {0x00, 0x01, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};

    uchar w[TOTAL][4];
    KeyExpansion(cypher_key, w);
    
    PrintAllW(w);  

    AES(in, out, w);
//    PrintAll(out);
    return 0;
}
