#include "encrypt.h"
using namespace std;

// Print All
void printAll(uchar (*result)[4]){
    int i, j;
    
    for(i=0; i<4; ++i){
        for(j=0; j<4; ++j)
            printf("%x ", result[i][j]);
        printf("\n");
    }
}
/*
   Shift Rows
*/
void shiftRows(uchar (*matrix)[4]){
    int i;
    for(i=1; i<4; ++i)
        rotate(matrix[i], i);
}

void rotate(uchar *row, int count){
    uchar ele;
    int i;
    while(count >0){
        ele = row[0];
        for(i=0; i<3; i++)
            row[i] = row[(i+1)%4];
        row[3] = ele;
        count -= 1;
    }
}

/*
   Mix Columns
*/
void mixColumns(uchar (*matrix)[4], uchar (*result)[4]){
    uchar temp[4], copy[4];
    int i, j;
    
    for(i=0; i<4; ++i){
        copy[0] = matrix[0][i];
        copy[1] = matrix[1][i];
        copy[2] = matrix[2][i];
        copy[3] = matrix[3][i];

        temp[0] = copy[0] & 0x80 ? (copy[0]<<1) ^ 0x1b : copy[0]<<1;
        temp[1] = copy[1] & 0x80 ? (copy[1]<<1) ^ 0x1b : copy[1]<<1;
        temp[2] = copy[2] & 0x80 ? (copy[2]<<1) ^ 0x1b : copy[2]<<1;
        temp[3] = copy[3] & 0x80 ? (copy[3]<<1) ^ 0x1b : copy[3]<<1;
        
        matrix[0][i] = temp[0]^copy[3]^copy[2]^temp[1]^copy[1];
        matrix[1][i] = temp[1]^copy[0]^copy[3]^temp[2]^copy[2];
        matrix[2][i] = temp[2]^copy[1]^copy[0]^temp[3]^copy[3];
        matrix[3][i] = temp[3]^copy[2]^copy[1]^temp[0]^copy[0];
    }

    // copy to result
    for(i=0; i<4; ++i){
        for(j=0; j<4; ++j){
            result[i][j] = matrix[i][j];
        }
    }
}

/*
void mixColumns(uchar (*matrix)[4], uchar (*result)[4]){
    int j;

    for(j=0; j<4; ++j)
        mixColumn(matrix, j, result);
}
*/
void mixColumn(uchar (*matrix)[4], int j, uchar (*result)[4]){
    int i;
    uchar temp[4];
    
    temp[0] = gf_mul[int(matrix[0][j])][0] xor gf_mul[matrix[1][j]][1] xor matrix[2][j] xor matrix[3][j];
    temp[1] = matrix[0][j] xor gf_mul[matrix[1][j]][0] xor gf_mul[int(matrix[2][j])][1] xor matrix[3][j];
    temp[2] = matrix[0][j] xor matrix[1][j] xor gf_mul[int(matrix[2][j])][0] xor gf_mul[int(matrix[3][j])][1];
    temp[3] = gf_mul[int(matrix[0][j])][1] xor matrix[1][j] xor matrix[2][j] xor gf_mul[int(matrix[3][j])][0];
 
    /*
    temp[0] = 0x02*matrix[0][j] + 0x03*matrix[1][j] + matrix[2][j] + matrix[3][j];
    temp[1] = 0x01*matrix[0][j] + 0x02*matrix[1][j] + 0x03*matrix[2][j] + 0x01*matrix[3][j];
    temp[2] = 0x01*matrix[0][j] + 0x01*matrix[1][j] + 0x02*matrix[2][j] + 0x03*matrix[3][j];
    temp[3] = 0x03*matrix[0][j] + 0x01*matrix[1][j] + 0x01*matrix[2][j] + 0x02*matrix[3][j];
    */
    for(i = 0; i<4; ++i)
        result[i][j] = temp[i];
}

/*
   Add Round Key
*/
void addRoundKey(uchar (*matrix)[4], uchar (*result)[4], int round, uchar ckey[][4], uchar (*newRoundKey)[4]){
    //uchar roundKey[4][4];
    int i, j;
    getKeycode(ckey, round, newRoundKey);

    for(j = 0; j<4; j++){
        for(i=0; i<4; i++){
            result[i][j] = matrix[i][j] ^ newRoundKey[i][j];
        }
    }
}

void addRoundKey(uchar (*matrix)[4], uchar (*result)[4], uchar (*roundKey)[4]){
    //uchar roundKey[4][4];
    int i, j;
    //getKeycode(ckey, round, roundKey);

    for(j = 0; j<4; j++){
        for(i=0; i<4; i++){
            result[i][j] = matrix[i][j] xor roundKey[i][j];
        }
    }
}

/*
   Encrypt
*/
void encrypt(uchar (*matrix)[4], uchar (*result)[4], uchar ckey[][4]){
    int i, j, round;
    
    uchar roundKeyArr[10][4][4];
    uchar interM[4][4];


    // Step 1: Add Round Key, round = -1
    addRoundKey(matrix, interM, ckey);
    
    for(round = 0; round<9; round++){
        // SubByte
        for(i=0; i<4; ++i)
            subBytes(interM[i]);

        //printAll(interM);
        //printf("-----\n");
        
        // ShiftRows
        shiftRows(interM);
        
        //printAll(interM);
        //printf("----\n");
        
        // Mix Columns
        mixColumns(interM, result);
        
        // Add Round Key
        if (round == 0)
            addRoundKey(result, interM, round, ckey, roundKeyArr[0]);
        else
            addRoundKey(result, interM, round, roundKeyArr[round-1], roundKeyArr[round]);
        //printAll(interM);
        //printf("---Abover is after Round Key--\n");
    }

    for(i=0; i<4; ++i)
        subBytes(interM[i]);
    shiftRows(interM);
    addRoundKey(interM, result, round, roundKeyArr[round-1], roundKeyArr[round]);
}
