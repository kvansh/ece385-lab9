#include "key_schedule.h"

void shiftRows(uchar (*matrix)[4]);
void rotate(uchar *row, int count);
void mixColumns(uchar (*matrix)[4], uchar (*result)[4]);
void mixColumn(uchar (*matrix)[4], int j, uchar (*result)[4]);
void addRoundKey(uchar (*matrix)[4], uchar (*result)[4], int round, uchar ckey[][4], uchar (*newRoundKey)[4]);
void addRoundKey(uchar (*matrix)[4], uchar (*result)[4], uchar roundKey[][4]);
void encrypt(uchar (*matrix)[4], uchar (*result)[4], uchar (*ckey)[4]);

void printAll(uchar (*result)[4]);
