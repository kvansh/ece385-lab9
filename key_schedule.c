#include "key_schedule.h"
using namespace std;

void getKeycode(uchar ckey[][4], int count, uchar (*result)[4]){
    int i,j;
    //uchar result[4][4];
    
    // Get Rotword
    uchar rotword[4] = {ckey[1][3], ckey[2][3], ckey[3][3], ckey[0][3]};

    subBytes(rotword);
    rcon(rotword,count,ckey);
    
    for (i=0; i<4; i++){
        result[i][0] = rotword[i];
    }

    for(i = 1; i<4; i++){
        for (j=0; j<4; ++j){
            result[j][i] = result[j][i-1]^ckey[j][i];
        }
    }
}

void rcon(uchar* rotword, int count, uchar (*ckey)[4]){
    int i, shift, temp;
    uchar _xor;
    shift = 24;
    
    for(i=0; i<4; ++i){
        temp = Rcon[count]>>shift;
        _xor = rotword[i] xor (uchar)(temp & 0xFF); 
        _xor = _xor xor ckey[i][0];
        //printf("rot: %x, aft: %x\n", rotword[i], _xor);
        rotword[i] = _xor;
        shift -= 8;
    }
}

void subBytes(uchar *rotword){
    int i;
    uchar upper4, lower4;

    for (i = 0; i<4; ++i){
        upper4 = (int)(rotword[i]) & 0xF0;
        upper4 = upper4>>4;

        lower4 = (int)(rotword[i]) & 0x0F;
        rotword[i] = aes_sbox[int(upper4)][int(lower4)];
    }
}

int getInt(uchar A){
    /*
    switch (A){
        case '0': return 0;
        case '1': return 1;
        case '2': return
    }*/
    return (int)(A)-48;
}
