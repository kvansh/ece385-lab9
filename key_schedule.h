#include "aes.h"

#define DEBUG 1

void getKeycode(uchar ckey[][4], int count, uchar (*result)[4]);
void subBytes(uchar* rotword);
int getInt(uchar A);
void rcon(uchar* rotword, int count, uchar (*ckey)[4]);
